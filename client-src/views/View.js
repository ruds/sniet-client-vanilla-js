export class View{

  constructor(viewName){
    this._table = document.querySelector(`#show-${viewName}-table`);
  }

  update(models){
    throw new Error("You must have implements this method.");
  }

  _template(models){
    throw new Error("You must have implements this method.");
  }

  showTable(show = false){
    this._table.style.display = show?"block":"none";
  }

  _updateSelectOptions(selectElementID, models, ...props){

    let select = document.querySelector(selectElementID);
    let html = ``;
    
    models.map(model=>{
      html += `
      <option value="${model[props[0]]}">${model[props[1]]}</option>
      `;
    });
    select.innerHTML += html;
  }
  
}