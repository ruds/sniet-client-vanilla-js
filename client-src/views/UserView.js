"use strict";

import { View } from "./View.js";

const ACCOUNT_COMUM_NUMBER = 3; //#account-type = [COMUM]
const VIEW_NAME = 'users';

export class UserView extends View{

  constructor(){
    super(VIEW_NAME);
    this._mapFields();
  }

  updateUsersTable(models){
    this._tableUsers.innerHTML += this._templateUsersTable(models);
    this.showTable(true);
  }

  _templateUsersTable(models){
    let html = ``;
    models.map(model=>{
      if(this._tdIds(model.idUsuario)){
        html += `<tr class="table-users-row">
            <td class="td-id">${model.idUsuario}</td>
            <td class="td-name">${model.name}</td>
            <td class="td-mail">${model.email}</td>
            <td class="td-inst-name">
              ${model.instituicao?model.instituicao.nome:" - "}
            </td>
          </tr>
        `;
      }
    })
    return html;
  }


  _tdIds(id){
    let tds = this._tableUsers.querySelectorAll('td.td-id');
    let control = true;
    tds.forEach(td => {
      if(id==td.textContent){
        control = false;
        return;
      }
    });
    return control;
  }

  changeRegisterType(value, models){
    if(value!=ACCOUNT_COMUM_NUMBER){
      this._academicInfos.classList.remove('invisible');
    }
    else{
      this._academicInfos.classList.add('invisible');
    }
  }

  setInstFields(instituition){

    console.log('setInstFields');

    let id = this._academicInfos
      .querySelector('#institute-id');
    let name = this._academicInfos
      .querySelector('#institute-name');
    let type = this._academicInfos
      .querySelector('#institute-type');
    let register = this._academicInfos
      .querySelector('#institute-register');
    
    if(instituition){
      id.value = instituition.id;
      name.value = instituition.nome;
      type.value = instituition.tipo;
      register.value = instituition.registro;

      type.setAttribute('disabled', 'disabled');
      register.setAttribute('disabled', 'disabled');
    }
    else{

      id.value = '';
      type.selectedIndex = 0;
      register.value = '';

      /*id.value = this._academicInfos
        .querySelector('#institute-id').value;
      type.value = this._academicInfos
        .querySelector('#institute-type').value;
      register.value = this._academicInfos
        .querySelector('#institute-register').value;*/

      type.removeAttribute('disabled');
      register.removeAttribute('disabled');
    }

  }


  _mapFields(){

    let userDoc = document.querySelector('#user-doc');

    if(userDoc){
      this._element = document.querySelector('#show-users-table');
      this._tableUsers = document.querySelector('#table-users-tbody');
      this._basicInfos = document.querySelectorAll('basic-infos');
      this._fullInfos = document.querySelector('#full-infos');
      this._academicInfos = document.querySelector('#academic-infos');
    }
    else return;

  }

}