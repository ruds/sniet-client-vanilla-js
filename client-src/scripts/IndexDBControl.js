"use strict";

const UPDATE_INDEXDB_IS_UPDATED = 'indexDbUpdated';

export class IndexDBControl {

  constructor(){
    throw new Error("This class can't be instatiated.");
  }

  static setIsUpdated(value){
    sessionStorage.setItem(UPDATE_INDEXDB_IS_UPDATED, value);
  }

  static isUpdated(){
    return sessionStorage.getItem(UPDATE_INDEXDB_IS_UPDATED) == "true"; 
  }

  static getValue(){
    return sessionStorage.getItem(UPDATE_INDEXDB_IS_UPDATED);
  }

  static indexDbIsUpdated(cb_isUpdated, cb_notUpdated=false){
    
    let result = undefined;

    if(IndexDBControl.isUpdated()){
      if(cb_isUpdated){
        console.log("isUpdated");
        result = cb_isUpdated();        
      }
    }
    else{
      if(cb_notUpdated){
        console.log("notUpdated");
        result = cb_notUpdated();
      }
    }
    IndexDBControl.setIsUpdated(true);
    return result;
  }

}