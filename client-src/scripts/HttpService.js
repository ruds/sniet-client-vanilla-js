export class HttpService {
  
  /* res.ok() verifica se houver algum erro na requisição */
  _handleErrors(resp) {
    if(!resp.ok) throw new Error(resp.statusText);
    return resp; 
  }
  
  //TODO - refazer verificando o 'Content-Type' da resposta 
  getJSON(url){
    return fetch(url)
    .then(resp => this._handleErrors(resp))
    .then(resp => resp.json())
    .catch(erro => console.log(erro));
  }

  //{ 'Content-Type': 'application/json' }
  post(url, data, headers){
    console.log(url, data, headers);
    return fetch(url,{
      headers: headers,
      method: 'post',
      body: JSON.stringify(data)
    })
    .then(resp => this._handleErrors(resp));
  }
 
  get(url){
    return fetch(url)
    .then(resp => this._handleErrors(resp))
    .catch(erro => console.log(erro));
  }

  getHeader(resp){
    return resp.headers;
  }
}