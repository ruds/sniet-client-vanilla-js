"use strict";

const stores = ['instituitions'];
const version = 4; 
const dbName = 'SHARK_DB';

let connection = null;
let close = null;

export class ConnectionFactory{

	constructor(){
		throw new Error('Não é possível criar instâncias dessa classe.');
	}

	static getConnection(keyPathValue, indexList){
		return new Promise((resolve, reject)=>{
			
			let openRequest = window.indexedDB.open(stores,version);

			openRequest.onupgradeneeded = (event)=>{
				console.log('Criando/alterando bancco.');
				ConnectionFactory._createStore(
					event.target.result, 
						keyPathValue,
						indexList
					);
			};

			openRequest.onsuccess = (event)=>{
				console.log('Obtendo conexão.');
				if(!connection){
					console.log('Criou nova conexão.');
					connection = event.target.result;
					close = connection.close.bind(connection);
					connection.close = ()=>{
					throw new Error('Essa conexão não pode ser fechada '
						+ 'diretamente.');
					}
				}
				else console.log('Usando conexão já existente.');
				resolve(connection);
			};

			openRequest.onerror = (event)=>{
				console.log('Falha ao criar/obter conexão.');
				console.log(event.target.error);
				reject(event.target.error.name);
			};

		});
	}

	static closeConnection(){
		if(connection){
			close();
			connection = null;
		}
	}

	static _createStore(conn, keyPathValue,indexList){
		stores.forEach((store)=>{
			if(conn.objectStoreNames.contains(store))
				conn.deleteObjectStore(store);
			
			let createdStore = conn.createObjectStore(
				store,
				{keyPath: keyPathValue, autoIncrement:true}
			);

			console.log(createdStore);
			
			if(indexList.length > 0)
				ConnectionFactory._createIndex(createdStore, indexList);
		});
	}

	static _createIndex(store, indexList = []){
		indexList.forEach((index)=>{
			store.createIndex(index.name, index.key,
				{ 
					multiEntry: true,
					unique: false, 
					}
				);
		});
	}

}