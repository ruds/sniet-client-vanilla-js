"user strict";

export class AddEvent{
  static addEventListener(eventName, target, callback){
    if(target){
      target.addEventListener(eventName, event=>{
        if(callback) callback();
        else return;
      })
    }
  }
}