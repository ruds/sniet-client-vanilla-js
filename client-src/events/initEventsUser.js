import { UserControllerProxy } 
  from "../controller/UserController.js";

import { AddEvent } from "./AddEvent.js";

export function initUserEvents() {
  USER_EVENTS.initUserEvents();  
}

const USER_EVENTS = {
  
  controller: undefined,

  initUserEvents: function() {
    USER_EVENTS.controller = new UserControllerProxy();
    USER_EVENTS.getUsers();
    USER_EVENTS.addUser();
    USER_EVENTS.closeUserTable();
    USER_EVENTS.renderizeFormAccount();
    USER_EVENTS.loadInstituitionByName();
    USER_EVENTS.controller.loadInstituition();
  },

  getUsers: function(){
    AddEvent.addEventListener(
      'click',
      document.querySelector('#btn-fetch'),
      ()=>{USER_EVENTS.controller.fetchAllUsers();}
    );
  },
  
  addUser: function(){
    AddEvent.addEventListener(
      'click',
      document.querySelector('#btn-save'),
      ()=>{USER_EVENTS.controller.saveUser()}
    );
  },
  
  closeUserTable: function(){
    AddEvent.addEventListener(
      'click',
      document.querySelector('#show-users-table span.btn-close-table'),
      ()=>{USER_EVENTS.controller.showTable(false)}
    );
  },
  
  renderizeFormAccount: function () {

    let element = document.querySelector('#account-type');

    AddEvent.addEventListener(
      'change',
      document.querySelector('#account-type'),
      ()=>{USER_EVENTS.controller.renderizeForm(element.value)}
    );
  },

  loadInstituitionByName : function() {
    let element = document.querySelector('#institute-name');
    AddEvent.addEventListener(
      'blur',
      element,
      ()=>{
        USER_EVENTS.controller
        .loadInstituitionByName(element.value);
      }
    );
  }

}