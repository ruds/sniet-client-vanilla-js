import { loginController } 
	from "../controller/LoginController.js";

import { AddEvent } from "./AddEvent.js";

export function initLoginEvents() {
	LOGIN_EVENTS.login();
}

const LOGIN_EVENTS = {

	controller: loginController,

	login: ()=>{
		AddEvent.addEventListener(
			'click', 
			document.querySelector('#btn-login'), 
			()=>{LOGIN_EVENTS.controller.login();});
	}, 
}