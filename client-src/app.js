"use strict";

import { Router } from "./router/Router.js";
import { Page } from "./router/Page.js";
import { Layout } from "./router/Layout.js";
import { initLoginEvents } from "./events/initEventsLogin.js";
import { initUserEvents } from "./events/initEventsUser.js";
import { clientSocket } from "./ws_sockets/ws_peoples.js";

clientSocket();

var routes = {
	login: {
		page: new Layout(new Page('login.html')), 
		callbacks: [initLoginEvents]
	},
	user: { 
		page: new Layout(new Page('user.html')), 
		callbacks: [initUserEvents]
	},
	'#default': {
		page: new Layout(new Page('login.html')), 
		callbacks: [initLoginEvents]
	},
};

const r = new Router(routes, document.querySelector('main'));