"use strict";

export class Page {

  constructor(url) {
    this.url = '' + url;
    this.htm = undefined;
  }

  load() {
    return $.get(this.url).then(res => this.html = res);
  }  

  show(el) {
    el.innerHTML = this.html;
  }

}