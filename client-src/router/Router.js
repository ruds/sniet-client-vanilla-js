"use strict";

export class Router {
  
  constructor(routes, el) {
    this.routes = routes;
    this.el = el;
    window.onhashchange = this.hashChanged.bind(this);
    this.hashChanged();
  }

  async hashChanged(ev) {
    if (window.location.hash.length > 0) {
      const pageName = window.location.hash.substr(1);
      this.show(pageName);
    } else if (this.routes['#default']) {
      this.show('#default');
    }
  }

  async show(pageName) {
    let callbacks = this.routes[pageName].callbacks;
    const page = this.routes[pageName].page;
    await page.load();
    this.el.innerHTML = '';
    page.show(this.el);
    this._executeCallbacks(callbacks);
  }

  hashChangedPromise(ev) {
    if (window.location.hash.length > 0) {
      const pageName = window.location.hash.substr(1);
      this.showPromise(pageName);
    } else if (this.routes['#default']) {
      this.show('#default');
    }
  }

  showPromise(pageName) {
    return new Promise((res, rej)=>{
      const page = this.routes[pageName];
      res(()=>{
        page.load();
        this.el.innerHTML = '';
        page.show(this.el);
      });
      rej(new Error());
    });
  }

  _executeCallbacks(callbacks=[]){
    callbacks.forEach(cb=>{
      if(cb) cb();
      else return;
    });
  }

}