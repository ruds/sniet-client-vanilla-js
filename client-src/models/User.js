"use strict";

export class User {

  constructor(idUsuario, instituition, name, secondName, sex,
    acessLevel, email, phone, login, password, status) {

    this.idUsuario = idUsuario;
    this.instituition = instituition;
    this.name = name;
    this.secondName = secondName;
    this.sex = sex;
    this.acessLevel = acessLevel;
    this.email = email;
    this.phone = phone;
    this.login = login;
    this.password = password;
    this.status = status;
  }

}