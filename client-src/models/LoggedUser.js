"use strict";

class LoggedUser{

	constructor(id, name, acessLevel, email, token){
		this.id = id;
		this.name = name;
		this.acessLevel = acessLevel;
		this.email = email;
		this.token = token;
	}
	
	get id(){
		return this.id;
	}

	get acessLevel(){
		return this.acessLevel;
	}

	get email(){
		return this.email;
	}
	
	get name(){
		return this.name;
	}
	
	get token(){
		return this.token;
	}


}