"use strict";

export class Instituition {

  constructor(idInstituition, name, register, instituitionType) {
    this.idInstituition = idInstituition;
    this.name = name;
    this.register = register;
    this.instituitionType = instituitionType;
  }

}