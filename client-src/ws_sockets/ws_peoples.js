const socket = new WebSocket("ws://localhost:8282/sniet/ws/test");

/* import { MessageView } from "gen_view/MessageView.js";
import { PeopleView } from "gen_view/PeopleView.js";
import { PeopleSocketControl } from "ws_sockets/controllers/PeopleSocketControl.js";

const controller =
 new PeopleSocketControl(new PeopleView(document.querySelector('#show-peoples')),
 new MessageView(document.querySelector('#show-messages'))); */


export var clientSocket = ()=>{

  // console.log('Init client-socket');

  socket.onopen = function () {
    //controller.addMessage('Connection Established!','info');
    // console.log('Connection Established!');
    socket.send(JSON.stringify({id:undefined, data:"Hello! I'm a client!"}));
  };

  socket.onclose = function () {
    console.log('Connection Closed!');
    //controller.addMessage('Server connection lost.', 'error', -1);
  };

  socket.onerror = function (error) {
    console.log(error);
  };

  socket.onmessage = function (e) {
    if (typeof e.data === "string") {

      let obj = JSON.parse(e.data);

      switch (obj.type) {
        case "data":
          console.log(obj.data);
        break;

        case "message":
          // controller.addMessage(obj.data);
          console.log(obj.data);
        break;

        case "app-info":
          console.log(obj.data);
        break;

        default:
          console.log('Unknow data type!');
          throw new Error('Unknow data type!');
        break;
      }
    }
    else if (e.data instanceof ArrayBuffer) {
      console.log('ArrayBuffer received: ' + e.data);
    }
    else if (e.data instanceof Blob) {
      console.log('Blob received: ' + e.data);
    }
  };
};