"use strict";

import { UserView } from "../views/UserView.js";
import { UserService } from "../service/UserService.js";
import { InstituitionService } from "../service/InstituitionService.js";
import { ProxyFactoryXP } from "../scripts/ProxyFactoryXP.js"

const userService = new UserService();
const instituitionService = new InstituitionService();

export class UserController{
    
  constructor(){
    this._userView = new UserView();
  }

  fetchAllUsers(){
    return new Promise((resolve, reject)=>{
      try{
        return userService
        .fetchAllExternalDB().then(data=>{
          this._showResult(data);
          return resolve();
        });    
      }
      catch(error) {
        console.log(error);
        return reject(error);
      }
    });
  }

  _showResult(data){
    this._userView.updateUsersTable(data);
  }

  showTable(data){    
    this._userView.showTable(data);
  }

  saveUser(){
    userService.save();
  }

  loadInstituitionByName(name){
    instituitionService.fetchBy(name).then((data)=>{
      if(data.length>0)
        data.forEach(element => {
          this._userView.setInstFields(element);
        });
      else this._userView.setInstFields(undefined);
    });
  }

  renderizeForm(value){
    instituitionService.fetchAllInternal().then((data)=>{
      this._userView.changeRegisterType(value, data);
    });
  }
  
  loadInstituition(){
    instituitionService.loadInstituition_IndexDB();
  }
  
  removeUser(element){}

  trapOne(){
    console.log('Test trap one');
  }

  trapTwo(){
    console.log('Test trap two');
  }

  trapTree(param){
    console.log('Test trap tree: ' + param);
  }

}

let controller = new UserController();

let trapsFetch = [()=>controller.trapOne()];
let trapsRemove = [()=>controller.trapTwo(), ()=>controller.trapTree('Hello!')];

let fetchPeopleTrap = {triggerName: '_updatePeople', traps: trapsFetch};
let removePeopleTrap = {triggerName: 'removePeople', traps: trapsRemove};

export let UserControllerProxy = ProxyFactoryXP.createClass(controller, fetchPeopleTrap,
  removePeopleTrap);