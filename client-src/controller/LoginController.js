"use strict";

import { LoginService } from "../service/LoginService.js";
import { ProxyFactoryXP } from "../scripts/ProxyFactoryXP.js"

const loginService = new LoginService();

export class LoginController{
    
  constructor(){ }

  login(){
    let loginPrommise = loginService.login();
    loginPrommise.then(data=>{
      loginService.createSession(data.user, data.token);
      this._redirect();
    });
  }

  _redirect(){
    window.location.replace("#user");
  }

}

let controller = new LoginController();

export let loginController = ProxyFactoryXP.create(controller);