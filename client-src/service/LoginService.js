"use strict";

import { Service } from "./Service.js";
import { HttpService } from "../scripts/HttpService.js";

const HttpService_ = new HttpService(); 
const USER_URI = 'http://localhost:8282/sniet/usuarios/login';

export class LoginService extends Service{
  
  constructor() { 
    super(USER_URI);
    this._fields = this._mapFields();
  }
  
  login(){
    let headers = { 'Content-Type': 'application/json' };
    let promise = 
      HttpService_.post(USER_URI, this._create(), headers); 
    // let headers = undefined;

    return promise
    .then(resp=>{
      headers = resp.headers;
      return resp.json()
    })
    .then((json)=>{
      let auth = headers.get('authorization');
      return {user: json, token: auth};
    })
    .catch(error=>console.error(error));

  }

  _create(){
    
    this._mapFieldValues(this._mapFields());

    let loginObj = {
      login: this._fields.get('login'),
      password: this._fields.get('password')
    };
    return loginObj;
  }

  _mapFields(){
    let fields = new Map();
    fields.set('login',document.querySelector('#login'));
    fields.set('password',document.querySelector('#password'));
    return fields;
  }


  /**
   * Add user infos and token in the session scope.
   * 
   * @param user JS object containing user data 
   * @param token token value;
   */
  createSession(user, token){
    sessionStorage.setItem('user',JSON.stringify(user));
    sessionStorage.setItem('token', token);
  }

}