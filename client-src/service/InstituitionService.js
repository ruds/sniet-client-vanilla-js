"use strict";

import { Service } from "./Service.js";
import { InstituitionDao } from "../dao/InstituitionDao.js";
import { IndexDBControl } from "../scripts/IndexDBControl.js";

const INSTITUITION_URI = 'http://localhost:8282/sniet/usuarios/instituition/';
const DAO = new InstituitionDao();

export class InstituitionService extends Service{

  constructor() { 
    super(INSTITUITION_URI);
  }

  loadInstituition_IndexDB(){
    IndexDBControl.indexDbIsUpdated( 
      ()=>{},
      ()=>{this._updateInternalDb(DAO)}
    );
  }

  fetchAllInternal(){
    return this.fetchAllInternalDB(DAO).then(data=>data);
  }

  fetchBy(value){
    return DAO.findByInstName(value);
  }

}