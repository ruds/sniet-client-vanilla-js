"use strict";

import { Service } from "./Service.js";
import { HttpService } from "../scripts/HttpService.js";
import { User } from "../models/User.js";
import { Instituition } from "../models/Instituition.js";

const HttpService_ = new HttpService(); 
const USER_URI = 'http://localhost:8282/sniet/usuarios';

export class UserService extends Service{
    
  constructor() { 
    super(USER_URI);
  }
  
  add(data = []){
    this._add(data, updateIndexDB);
  }

  save(){

    let token = sessionStorage.getItem('token');
    let headers = { 
      'Content-Type': 'application/json',
      'Authorization': token,
    };
    console.log(this._create());
    HttpService_.post(USER_URI, this._create(), headers);
  }

  _create(){

    this._mapFieldValues(this._mapFields());
    
    let inst = this._fields.get('inst_name')==null
      ?undefined  
      :new Instituition(this._fields.get('inst_id'), this._fields.get('inst_name'),
        this._fields.get('inst_register'), this._fields.get('inst_type'));

    return new User(this._fields.get('id'), inst, this._fields.get('name'),
      this._fields.get('secondName'),this._fields.get('sex'),
      this._fields.get('acessLevel'),this._fields.get('email'),this._fields.get('phone'),
      this._fields.get('login'), this._fields.get('password'),this._fields.get('status'));

  }


  _mapFields(){

    let fields = new Map();

    fields.set('id',document.querySelector('#user-id'));
    fields.set('name',document.querySelector('#name'));
    fields.set('secondName',document.querySelector('#second-name'));
    fields.set('sex',document.querySelector('#sex'));
    fields.set('acessLevel',document.querySelector('#account-type'));
    fields.set('email',document.querySelector('#email'),);
    fields.set('phone',document.querySelector('#phone'));
    fields.set('login',document.querySelector('#login'));
    fields.set('password',document.querySelector('#password'));
    fields.set('status',document.querySelector('#account-status'));

    fields.set('inst_id',document.querySelector('#institute-id'));
    fields.set('inst_name',document.querySelector('#institute-name'));
    fields.set('inst_register',document.querySelector('#institute-register'));
    fields.set('inst_type',document.querySelector('#institute-type'));

    return fields;
  }

}