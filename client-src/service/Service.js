"use strict";

import { HttpService } from "../scripts/HttpService.js";

const HttpService_ = new HttpService();

export class Service{
  
  constructor(uri) {
    this._uri = uri;
    this._fields = undefined;
  }
 
  _addIndexDB(data = [], dao){
    data.map(item=>dao.add(item));
  }
  
  fetchAllInternalDB(dao){

    return new Promise((resolve, reject)=>{

      try {
        return dao.listAll().then(data=>resolve(data));
      }
      catch(error){
        console.log(error)
        reject(error);
      }

    });

  }
  
  fetchAllExternalDB(){
    return new Promise((resolve, reject)=>{
      try{
        let promise = HttpService_.getJSON(this._uri);
        return resolve(promise.then(data=>{
          return data;
        }));
      }
      catch(error) { 
        console.log(error);
        return reject(error);
      }
    });
  }

  _updateInternalDb(dao){
    this.fetchAllExternalDB().then(data => {
      this._addIndexDB(data,dao);
    });
  }


  _mapFieldValues(mappedFields){
    this._fields = mappedFields; 

    for(var key of this._fields.keys()){
      if(this._fields.get(key)==null)
        this._fields.set(key,undefined);
      else
        this._fields.set(key, this._fields.get(key).value 
        || this._fields.get(key).text);
    }
  }

  _mapFields(){
    throw new Error("Must be implemented.");
  }

  _create(){
    throw new Error("Must be implemented.");
  }

}