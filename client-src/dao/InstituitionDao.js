"use strict";

import { Dao } from "./Dao.js";

const STORE_NAME = 'instituitions';
const KEY_PATH = 'id';

const INDEX_LIST = [
  {name: "instName", key:"nome"}
];

export class InstituitionDao extends Dao {

  constructor(){
    super(STORE_NAME, INDEX_LIST, KEY_PATH, InstituitionDao);
  }
  
  findByInstName(value){
    return this._findAllOcurrences("instName", value)
    .then(data=>{
      return data;
    });
  }

}