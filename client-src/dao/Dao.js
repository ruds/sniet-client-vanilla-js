"use strict";

import { ConnectionFactory } from "../scripts/ConnectionFactory.js";

export class Dao{

	constructor(storeName, indexList, keyPathValue, type){
		this._store = storeName;
		this._indexList = indexList;
		this._keyPathValue = keyPathValue
		this._type = type;
	}

	add(data){
		ConnectionFactory.getConnection()
		.then((conn)=>{
			conn.transaction([this._store],'readwrite')
			.objectStore(this._store)
			.add(data);
		});
	}

	listAll() {

		return ConnectionFactory.getConnection()
		.then((conn)=>{

			return new Promise((resolve, reject)=>{

				let results = [];
				let cursor = conn.transaction([this._store],'readwrite')
				.objectStore(this._store).openCursor();

				cursor.onsuccess = (event)=>{
					let atual = event.target.result;

					if(atual){
						let atualdata = atual.value;
						results.push(atualdata);
						atual.continue();
					}else{
						return resolve(results);
					}
				};

				cursor.onerror = (event)=>{
					console.log(event.target.error.name);
					return event.target.error.name;
				};

			});
		});
	}

	_findAllOcurrences(indexName, value){

		return ConnectionFactory.getConnection()
		.then((conn)=>{

			return new Promise((resolve, reject)=>{
			
				let results = [];

				//Recuperando index (anteriormente criado)
				let index = conn
				.transaction([this._store],'readonly')
				.objectStore(this._store).index(indexName);

				// Case sensitive
				let request = index.openCursor(IDBKeyRange.only(value));

				request.onsuccess = ()=>{
					let cursor = request.result;
					if(cursor){
						results.push(cursor.value);
						cursor.continue();
					}
					else return resolve(results);
				}

				request.onerror = (event)=>{
					let err = event.target.error;
					console.log(err);
					return reject(err);
				};

			});
		});
	}

	deleteAll(){
		return ConnectionFactory.getConnection()
		.then((conn)=>{
			
			return new Promise((resolve, reject) => {
						
				let request = conn
					.transaction([this._store],'readwrite')
					.objectStore(this._store)
					.clear();

				request.onsuccess = (event)=>{
					resolve()
				};

				request.onerror = (event)=>{
					reject(event.target.error);
				};

			});
		})
	}

	getOneById(id){
		return ConnectionFactory.getConnection()
		.then((conn)=>{
			return new Promise((resolve, reject)=>{
					
				let request = conn
					.transaction([this._store],'readwrite')
					.objectStore(this._store).get(id);

				request.onsuccess = (event)=>{
					resolve(request.result);
				};

				request.onerror = (event)=>{
					reject(event.target.error);
				};
				
			});
		})
	}

	size(){
		return this._connection
		.transaction([this._store],'readonly')
		.objectStore(this._store).count();
	}

}